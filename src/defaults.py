# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (C) 2009-2013 Stephan Raue (stephan@openelec.tv)
# Copyright (C) 2013 Lutz Fiebach (lufie@openelec.tv)
# Copyright (C) 2019 Corey Moyer (cronmod.dev@gmail.com)

import os

################################################################################
# Base
################################################################################

KODI_USER_HOME = os.environ.get('KODI_USER_HOME', '/root/.kodi')
CONFIG_CACHE = os.environ.get('CONFIG_CACHE', '/root/.cache')
USER_CONFIG = os.environ.get('USER_CONFIG', '/root/.config')

################################################################################
# Connamn Module
################################################################################

connman = {
    'CONNMAN_DAEMON': '/usr/sbin/connmand',
    'WAIT_CONF_FILE': '%s/kodi/network_wait' % CONFIG_CACHE,
    'ENABLED': lambda : (True if os.path.exists(connman['CONNMAN_DAEMON']) else False),
    }

################################################################################
# Bluez Module
################################################################################

bluetooth = {
    'BLUETOOTH_DAEMON': '/usr/libexec/bluetooth/bluetoothd',
    'OBEX_DAEMON': '/usr/libexec/bluetooth/obexd',
    'ENABLED': lambda : (True if os.path.exists(bluetooth['BLUETOOTH_DAEMON']) else False),
    'D_OBEXD_ROOT': '/root/Downloads/',
    }

################################################################################
# Service Module
################################################################################

services = {
    'ENABLED': True,
    'KERNEL_CMD': '/proc/cmdline',
    'SAMBA_NMDB': '/usr/sbin/nmbd',
    'SAMBA_SMDB': '/usr/sbin/smbd',
    'D_SAMBA_WORKGROUP': 'WORKGROUP',
    'D_SAMBA_SECURE': '0',
    'D_SAMBA_USERNAME': 'user',
    'D_SAMBA_PASSWORD': 'passwd',
    'D_SAMBA_MINPROTOCOL': 'SMB2',
    'D_SAMBA_MAXPROTOCOL': 'SMB3',
    'D_SAMBA_AUTOSHARE': '1',
    'SSH_DAEMON': '/usr/sbin/dropbear',
    'OPT_SSH_NOPASSWD': '-s',
    'D_SSH_DISABLE_PW_AUTH': '0',
    'AVAHI_DAEMON': '/usr/sbin/avahi-daemon',
    'CRON_DAEMON': '/sbin/crond',
    }

system = {
    'ENABLED': True,
    'KERNEL_CMD': '/proc/cmdline',
    'SET_CLOCK_CMD': '/sbin/hwclock --systohc --utc',
    'KODI_RESET_FILE': '%s/reset_kodi' % CONFIG_CACHE,
    'EMBER_RESET_FILE': '%s/reset_ember' % CONFIG_CACHE,
    'KEYBOARD_INFO': '/usr/share/X11/xkb/rules/base.xml',
    'UDEV_KEYBOARD_INFO': '%s/xkb/layout' % CONFIG_CACHE,
    'NOX_KEYBOARD_INFO': '/usr/share/bkeymaps',
    'BACKUP_DIRS': [
        KODI_USER_HOME,
        USER_CONFIG,
        CONFIG_CACHE,
        '/root/.ssh',
        ],
    'BACKUP_DESTINATION': '/root/Backups/',
    'RESTORE_DIR': '/root/.restore/',
    }

updates = {
    'ENABLED': False,
    'UPDATE_REQUEST_URL': 'https://update.libreelec.tv/updates.php',
    'UPDATE_DOWNLOAD_URL': 'http://%s.libreelec.tv/%s',
    'LOCAL_UPDATE_DIR': '/root/.update/',
    }

about = {'ENABLED': False}

xdbus = {'ENABLED': True}

_services = {
    'sshd': ['dropbear.service'],
    'avahi': ['avahi-daemon.socket', 'avahi-daemon.service', 'avahi-dnsconfd.service'],
    'samba': ['samba.service'],
    'bluez': ['bluetooth.service'],
    'obexd': ['obex.service'],
    'crond': ['cron.service'],
    'iptables': ['iptables.service'],
    }
